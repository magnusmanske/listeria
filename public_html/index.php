<?PHP

ini_set('memory_limit','4000M');
set_time_limit ( 60 * 15 ) ; // Seconds

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR|E_ALL);
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
require_once ( '../shared.inc' ) ;

//header('Content-type: text/html; charset=UTF-8');
$header = get_common_header ( '' , 'Listeria' ) ;
$header = str_replace('//bitbucket.org/magnusmanske','//github.com/magnusmanske/listeria_rs',$header);
print $header ;
print "<div style='float:right'>
<a href='https://commons.wikimedia.org/wiki/File:Listeria_monocytogenes_01.jpg' target='_blank' title='Listeria monocytogenes. Biology pun. Sorry.'>
<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/Listeria_monocytogenes_01.jpg/160px-Listeria_monocytogenes_01.jpg' border=0 />
</a></div>" ;

function getTimestamp() {
	return date ( 'YmdHis' ) ;
}

$action = get_request ( 'action' , '' ) ;
$testing = isset($_REQUEST['testing']) ;

if ( $action == 'update' ) {
	$project = get_request ( 'project' , '' ) ;
	$lang = get_request ( 'lang' , $project ) ;
	$page = get_request ( 'page' , '' ) ;
	
	print "<p>Updating ".htmlentities($page)."...</p>\n" ; myflush() ;

	$starttime = microtime(true);
	if ( $project != '' and $project != 'wikipedia' and $project != 'wikidata' ) $l = new Listeria ( $lang , $project ) ;
	else $l = new Listeria ( $lang ) ;

	$tool_db = openToolDB ( 'listeria_bot' ) ;
	$tool_db->set_charset("utf8") ;

	$wiki = $l->wiki;
	$wiki_safe = $tool_db->real_escape_string($wiki);
	$page_safe = $tool_db->real_escape_string( str_replace ( ' ' , '_' , $page ) ) ;
	$sql = "INSERT INTO pagestatus (`wiki`,`page`,`status`,`query_sparql`,`priority`) VALUES ((SELECT id FROM wikis WHERE name='{$wiki_safe}'),'{$page_safe}','WAITING','',1) ON DUPLICATE KEY UPDATE `priority`=1" ;
	// print "<p>{$sql}</p>"; myflush() ;
	if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $this->db->error . ']');
	$pagestatus_id = $tool_db->insert_id;
	$sql = "SELECT * FROM pagestatus WHERE id={$pagestatus_id} AND priority=0" ;
	// print "<p>{$sql}</p>"; myflush() ;

	$still_has_priority = true;
	while ( $still_has_priority ) {
		sleep(2);
		if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $this->db->error . ']');
		while($o = $result->fetch_object()) $still_has_priority = false;
		if ( $still_has_priority ) {
			// print "<p>WAITING...</p>\n" ; myflush();
		}
	}

	$endtime = microtime(true);
	$timediff = $endtime - $starttime;
	print "<p>{$timediff} seconds elapsed.</p>"; myflush();

	
	$sql = "SELECT status,message FROM pagestatus WHERE id={$pagestatus_id}" ;
	if(!$result = $tool_db->query($sql)) die('There was an error running the query [' . $this->db->error . ']');
	if($o = $result->fetch_object()) {
		print "<p><b>Result:</b> {$o->status} <i>{$o->message}</i></p>";
	}
	
	
	print "<p>Return to <a href='https://" . $server . "/wiki/" . myurlencode($page) . "'><b>".htmlentities($page)."</b></a></p>" ;
	print "<p>Also, check out the <a href='./botstatus.php'>current bot status!</a></p>" ;

	$tool_db = openToolDB ( 'listeria_bot' ) ;
	$tool_db->set_charset("utf8") ;
	$status = isset($l->error) ? $l->error : 'OK' ;
	// print "<p>Status: $status</p>" ;
	$sql = "SELECT * FROM wikis WHERE name='".$tool_db->real_escape_string($l->wiki)."'" ;
	if(!$result2 = $tool_db->query($sql)) die('There was an error running the query [' . $tool_db->error . ']');
	while($o = $result2->fetch_object()) $wiki_id = $o->id ;
	$page_db = $tool_db->real_escape_string ( str_replace(' ','_',$page) ) ;
	if ( isset($wiki_id) ) {
		$sql = "REPLACE INTO pagestatus (wiki,page,status,timestamp) VALUES ('$wiki_id','$page_db','".$tool_db->real_escape_string($status)."','".getTimestamp()."')" ;
		if(!$result2 = $tool_db->query($sql)) print('There was an error running the query [' . $tool_db->error . ']');
	}

} else if ( $action == 'get_wikitext' ) {
	$lang = get_request ( 'lang' , '' ) ;
	$page = get_request ( 'page' , '' ) ;
	$l = new Listeria ( $lang ) ;
	$l->loadPage ( $page ) ;
	$w = $l->getWikiText() ;
	print "<pre>".htmlentities($w)."</pre>" ;

} else {
?>
<div class='lead'>
<p><a href='https://www.wikidata.org/wiki/User:ListeriaBot'>This bot</a> can generate and update lists on Wikipedia, based on <a href='https://query.wikidata.org/'>Wikidata queries</a>.</p>
<p>Check out <a href='http://magnusmanske.de/wordpress/?p=301'>my original blog post</a> explaining how this works, and the <a href='http://magnusmanske.de/wordpress/?p=650'>blog post for the current version 2</a>.</p>
<p><a href='botstatus.php'>Bot status</a> overview</p>
<p><a href='dynamic.html'>Dynamic version</a></p>
</div>
<?PHP
}

print get_common_footer() ;

?>