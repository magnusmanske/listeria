#!/usr/bin/php
<?PHP

require_once ( '/data/project/listeria/public_html/php/ToolforgeCommon.php' ) ;

$tfc = new ToolforgeCommon ( 'listeria' ) ;

$db = $tfc->openDBtool('listeria_p') ;
$sql = "SELECT sparql,group_concat(id) AS ids FROM list GROUP BY sparql" ;
$result = $tfc->getSQL($db,$sql) ;
while($o = $result->fetch_object()) {
	try {
		$sparql = $o->sparql ;
		if ( trim($sparql) == '' ) continue ;

		# Detect first variable name
		$varname = 'item' ; # Default
		if ( preg_match ( '/\?([a-zA-Z0-9_-]+)/' , str_replace("\n",' ',$sparql) , $m ) ) $varname = $m[1] ;
		#print "{$varname}: {$sparql}\n" ;
		$items = '' ;
		$noi = 0 ;
		foreach ( $tfc->getSPARQL_TSV($sparql) AS $o ) {
			$noi++ ;
			if ( $items=='' ) $items = $o[$varname] ;
			else $items .= ','.$o[$varname] ;
		}

		$sql = "UPDATE list SET last_update='" . date('c') . "',items='{$items}',number_of_items={$noi} WHERE id IN ({$o->ids})" ;
		#print "{$sql}\n\n" ;
		$db->query($sql);
	} catch (Exception $e) {
		echo $e->getMessage() ;
   	}
}

?>